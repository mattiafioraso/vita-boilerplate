import {
  FieldPolicy,
  FieldReadFunction,
  TypePolicies,
  TypePolicy,
} from '@apollo/client/cache';
export type CityKeySpecifier = (
  | 'continent'
  | 'country'
  | 'geonamesID'
  | 'id'
  | 'location'
  | 'name'
  | 'population'
  | 'timeZone'
  | 'timeZoneDST'
  | CityKeySpecifier
)[];
export type CityFieldPolicy = {
  continent?: FieldPolicy<any> | FieldReadFunction<any>;
  country?: FieldPolicy<any> | FieldReadFunction<any>;
  geonamesID?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  location?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  population?: FieldPolicy<any> | FieldReadFunction<any>;
  timeZone?: FieldPolicy<any> | FieldReadFunction<any>;
  timeZoneDST?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ClientKeySpecifier = (
  | 'ipAddress'
  | 'userAgent'
  | ClientKeySpecifier
)[];
export type ClientFieldPolicy = {
  ipAddress?: FieldPolicy<any> | FieldReadFunction<any>;
  userAgent?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type ContinentKeySpecifier = (
  | 'countries'
  | 'geonamesID'
  | 'id'
  | 'name'
  | 'population'
  | ContinentKeySpecifier
)[];
export type ContinentFieldPolicy = {
  countries?: FieldPolicy<any> | FieldReadFunction<any>;
  geonamesID?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  population?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CoordinatesKeySpecifier = (
  | 'lat'
  | 'long'
  | CoordinatesKeySpecifier
)[];
export type CoordinatesFieldPolicy = {
  lat?: FieldPolicy<any> | FieldReadFunction<any>;
  long?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CountryKeySpecifier = (
  | 'alpha2Code'
  | 'alpha3Code'
  | 'callingCodes'
  | 'capital'
  | 'cities'
  | 'continent'
  | 'currencies'
  | 'favorite'
  | 'geonamesID'
  | 'id'
  | 'languages'
  | 'location'
  | 'name'
  | 'population'
  | 'vatRate'
  | CountryKeySpecifier
)[];
export type CountryFieldPolicy = {
  alpha2Code?: FieldPolicy<any> | FieldReadFunction<any>;
  alpha3Code?: FieldPolicy<any> | FieldReadFunction<any>;
  callingCodes?: FieldPolicy<any> | FieldReadFunction<any>;
  capital?: FieldPolicy<any> | FieldReadFunction<any>;
  cities?: FieldPolicy<any> | FieldReadFunction<any>;
  continent?: FieldPolicy<any> | FieldReadFunction<any>;
  currencies?: FieldPolicy<any> | FieldReadFunction<any>;
  favorite?: FieldPolicy<any> | FieldReadFunction<any>;
  geonamesID?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  languages?: FieldPolicy<any> | FieldReadFunction<any>;
  location?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  population?: FieldPolicy<any> | FieldReadFunction<any>;
  vatRate?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type CurrencyKeySpecifier = (
  | 'convert'
  | 'countries'
  | 'id'
  | 'isoCode'
  | 'name'
  | 'unitSymbols'
  | CurrencyKeySpecifier
)[];
export type CurrencyFieldPolicy = {
  convert?: FieldPolicy<any> | FieldReadFunction<any>;
  countries?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  isoCode?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  unitSymbols?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type DNSRecordsKeySpecifier = (
  | 'a'
  | 'aaaa'
  | 'cname'
  | 'mx'
  | DNSRecordsKeySpecifier
)[];
export type DNSRecordsFieldPolicy = {
  a?: FieldPolicy<any> | FieldReadFunction<any>;
  aaaa?: FieldPolicy<any> | FieldReadFunction<any>;
  cname?: FieldPolicy<any> | FieldReadFunction<any>;
  mx?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type DomainNameKeySpecifier = (
  | 'a'
  | 'aaaa'
  | 'cname'
  | 'mx'
  | 'name'
  | 'records'
  | DomainNameKeySpecifier
)[];
export type DomainNameFieldPolicy = {
  a?: FieldPolicy<any> | FieldReadFunction<any>;
  aaaa?: FieldPolicy<any> | FieldReadFunction<any>;
  cname?: FieldPolicy<any> | FieldReadFunction<any>;
  mx?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  records?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type EmailAddressKeySpecifier = (
  | 'address'
  | 'domainName'
  | 'host'
  | 'local'
  | 'ok'
  | 'serviceProvider'
  | EmailAddressKeySpecifier
)[];
export type EmailAddressFieldPolicy = {
  address?: FieldPolicy<any> | FieldReadFunction<any>;
  domainName?: FieldPolicy<any> | FieldReadFunction<any>;
  host?: FieldPolicy<any> | FieldReadFunction<any>;
  local?: FieldPolicy<any> | FieldReadFunction<any>;
  ok?: FieldPolicy<any> | FieldReadFunction<any>;
  serviceProvider?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type EmailServiceProviderKeySpecifier = (
  | 'disposable'
  | 'domainName'
  | 'free'
  | 'smtpOk'
  | EmailServiceProviderKeySpecifier
)[];
export type EmailServiceProviderFieldPolicy = {
  disposable?: FieldPolicy<any> | FieldReadFunction<any>;
  domainName?: FieldPolicy<any> | FieldReadFunction<any>;
  free?: FieldPolicy<any> | FieldReadFunction<any>;
  smtpOk?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type HTMLDocumentKeySpecifier = (
  | 'all'
  | 'body'
  | 'first'
  | 'html'
  | 'title'
  | HTMLDocumentKeySpecifier
)[];
export type HTMLDocumentFieldPolicy = {
  all?: FieldPolicy<any> | FieldReadFunction<any>;
  body?: FieldPolicy<any> | FieldReadFunction<any>;
  first?: FieldPolicy<any> | FieldReadFunction<any>;
  html?: FieldPolicy<any> | FieldReadFunction<any>;
  title?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type HTMLNodeKeySpecifier = (
  | 'all'
  | 'attribute'
  | 'children'
  | 'first'
  | 'html'
  | 'next'
  | 'parent'
  | 'previous'
  | 'text'
  | HTMLNodeKeySpecifier
)[];
export type HTMLNodeFieldPolicy = {
  all?: FieldPolicy<any> | FieldReadFunction<any>;
  attribute?: FieldPolicy<any> | FieldReadFunction<any>;
  children?: FieldPolicy<any> | FieldReadFunction<any>;
  first?: FieldPolicy<any> | FieldReadFunction<any>;
  html?: FieldPolicy<any> | FieldReadFunction<any>;
  next?: FieldPolicy<any> | FieldReadFunction<any>;
  parent?: FieldPolicy<any> | FieldReadFunction<any>;
  previous?: FieldPolicy<any> | FieldReadFunction<any>;
  text?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type IPAddressKeySpecifier = (
  | 'address'
  | 'city'
  | 'country'
  | 'type'
  | IPAddressKeySpecifier
)[];
export type IPAddressFieldPolicy = {
  address?: FieldPolicy<any> | FieldReadFunction<any>;
  city?: FieldPolicy<any> | FieldReadFunction<any>;
  country?: FieldPolicy<any> | FieldReadFunction<any>;
  type?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type LanguageKeySpecifier = (
  | 'alpha2Code'
  | 'countries'
  | 'id'
  | 'name'
  | LanguageKeySpecifier
)[];
export type LanguageFieldPolicy = {
  alpha2Code?: FieldPolicy<any> | FieldReadFunction<any>;
  countries?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type MXRecordKeySpecifier = (
  | 'exchange'
  | 'preference'
  | MXRecordKeySpecifier
)[];
export type MXRecordFieldPolicy = {
  exchange?: FieldPolicy<any> | FieldReadFunction<any>;
  preference?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type MarkdownKeySpecifier = ('html' | MarkdownKeySpecifier)[];
export type MarkdownFieldPolicy = {
  html?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type QueryKeySpecifier = (
  | 'cities'
  | 'client'
  | 'continents'
  | 'countries'
  | 'currencies'
  | 'domainName'
  | 'emailAddress'
  | 'favoriteCountries'
  | 'htmlDocument'
  | 'ipAddress'
  | 'languages'
  | 'markdown'
  | 'random'
  | 'timeZones'
  | 'url'
  | QueryKeySpecifier
)[];
export type QueryFieldPolicy = {
  cities?: FieldPolicy<any> | FieldReadFunction<any>;
  client?: FieldPolicy<any> | FieldReadFunction<any>;
  continents?: FieldPolicy<any> | FieldReadFunction<any>;
  countries?: FieldPolicy<any> | FieldReadFunction<any>;
  currencies?: FieldPolicy<any> | FieldReadFunction<any>;
  domainName?: FieldPolicy<any> | FieldReadFunction<any>;
  emailAddress?: FieldPolicy<any> | FieldReadFunction<any>;
  favoriteCountries?: FieldPolicy<any> | FieldReadFunction<any>;
  htmlDocument?: FieldPolicy<any> | FieldReadFunction<any>;
  ipAddress?: FieldPolicy<any> | FieldReadFunction<any>;
  languages?: FieldPolicy<any> | FieldReadFunction<any>;
  markdown?: FieldPolicy<any> | FieldReadFunction<any>;
  random?: FieldPolicy<any> | FieldReadFunction<any>;
  timeZones?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type RandomKeySpecifier = (
  | 'float'
  | 'int'
  | 'string'
  | RandomKeySpecifier
)[];
export type RandomFieldPolicy = {
  float?: FieldPolicy<any> | FieldReadFunction<any>;
  int?: FieldPolicy<any> | FieldReadFunction<any>;
  string?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type TimeZoneKeySpecifier = (
  | 'cities'
  | 'id'
  | 'name'
  | 'offset'
  | TimeZoneKeySpecifier
)[];
export type TimeZoneFieldPolicy = {
  cities?: FieldPolicy<any> | FieldReadFunction<any>;
  id?: FieldPolicy<any> | FieldReadFunction<any>;
  name?: FieldPolicy<any> | FieldReadFunction<any>;
  offset?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type URLKeySpecifier = (
  | 'domainName'
  | 'host'
  | 'htmlDocument'
  | 'path'
  | 'port'
  | 'query'
  | 'scheme'
  | 'url'
  | URLKeySpecifier
)[];
export type URLFieldPolicy = {
  domainName?: FieldPolicy<any> | FieldReadFunction<any>;
  host?: FieldPolicy<any> | FieldReadFunction<any>;
  htmlDocument?: FieldPolicy<any> | FieldReadFunction<any>;
  path?: FieldPolicy<any> | FieldReadFunction<any>;
  port?: FieldPolicy<any> | FieldReadFunction<any>;
  query?: FieldPolicy<any> | FieldReadFunction<any>;
  scheme?: FieldPolicy<any> | FieldReadFunction<any>;
  url?: FieldPolicy<any> | FieldReadFunction<any>;
};
export type TypedTypePolicies = TypePolicies & {
  City?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | CityKeySpecifier | (() => undefined | CityKeySpecifier);
    fields?: CityFieldPolicy;
  };
  Client?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ClientKeySpecifier
      | (() => undefined | ClientKeySpecifier);
    fields?: ClientFieldPolicy;
  };
  Continent?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | ContinentKeySpecifier
      | (() => undefined | ContinentKeySpecifier);
    fields?: ContinentFieldPolicy;
  };
  Coordinates?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CoordinatesKeySpecifier
      | (() => undefined | CoordinatesKeySpecifier);
    fields?: CoordinatesFieldPolicy;
  };
  Country?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CountryKeySpecifier
      | (() => undefined | CountryKeySpecifier);
    fields?: CountryFieldPolicy;
  };
  Currency?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | CurrencyKeySpecifier
      | (() => undefined | CurrencyKeySpecifier);
    fields?: CurrencyFieldPolicy;
  };
  DNSRecords?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | DNSRecordsKeySpecifier
      | (() => undefined | DNSRecordsKeySpecifier);
    fields?: DNSRecordsFieldPolicy;
  };
  DomainName?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | DomainNameKeySpecifier
      | (() => undefined | DomainNameKeySpecifier);
    fields?: DomainNameFieldPolicy;
  };
  EmailAddress?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | EmailAddressKeySpecifier
      | (() => undefined | EmailAddressKeySpecifier);
    fields?: EmailAddressFieldPolicy;
  };
  EmailServiceProvider?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | EmailServiceProviderKeySpecifier
      | (() => undefined | EmailServiceProviderKeySpecifier);
    fields?: EmailServiceProviderFieldPolicy;
  };
  HTMLDocument?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | HTMLDocumentKeySpecifier
      | (() => undefined | HTMLDocumentKeySpecifier);
    fields?: HTMLDocumentFieldPolicy;
  };
  HTMLNode?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | HTMLNodeKeySpecifier
      | (() => undefined | HTMLNodeKeySpecifier);
    fields?: HTMLNodeFieldPolicy;
  };
  IPAddress?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | IPAddressKeySpecifier
      | (() => undefined | IPAddressKeySpecifier);
    fields?: IPAddressFieldPolicy;
  };
  Language?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | LanguageKeySpecifier
      | (() => undefined | LanguageKeySpecifier);
    fields?: LanguageFieldPolicy;
  };
  MXRecord?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | MXRecordKeySpecifier
      | (() => undefined | MXRecordKeySpecifier);
    fields?: MXRecordFieldPolicy;
  };
  Markdown?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | MarkdownKeySpecifier
      | (() => undefined | MarkdownKeySpecifier);
    fields?: MarkdownFieldPolicy;
  };
  Query?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | QueryKeySpecifier
      | (() => undefined | QueryKeySpecifier);
    fields?: QueryFieldPolicy;
  };
  Random?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | RandomKeySpecifier
      | (() => undefined | RandomKeySpecifier);
    fields?: RandomFieldPolicy;
  };
  TimeZone?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?:
      | false
      | TimeZoneKeySpecifier
      | (() => undefined | TimeZoneKeySpecifier);
    fields?: TimeZoneFieldPolicy;
  };
  URL?: Omit<TypePolicy, 'fields' | 'keyFields'> & {
    keyFields?: false | URLKeySpecifier | (() => undefined | URLKeySpecifier);
    fields?: URLFieldPolicy;
  };
};
