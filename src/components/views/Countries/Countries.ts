import { defineComponent, ref } from 'vue';

import { useResult } from '@vue/apollo-composable';

import {
  useCountriesQuery,
  useFavoriteCountriesQuery,
} from '../../../graphql/generated/graphql';

import { useSetFavoriteCountryAction } from '../../../graphql/apollo/store/Country/actions';

import Card from '../../shared/Card/Card.vue';

export default defineComponent({
  name: 'Countries',
  components: {
    Card,
  },
  setup() {
    const name = ref('');

    const countriesQuery = useCountriesQuery(
      () =>
        name.value
          ? {
              where: {
                name: {
                  eq: name.value,
                },
              },
            }
          : {},
      { debounce: 200 },
    );

    const favoriteCountriesQuery = useFavoriteCountriesQuery();

    const countries = useResult(
      countriesQuery.result,
      [],
      (result) => result.countries,
    );

    const favoriteCountries = useResult(
      favoriteCountriesQuery.result,
      [],
      (result) => result.favoriteCountries,
    );

    const setFavorite = useSetFavoriteCountryAction();

    return {
      name,
      setFavorite,
      countries,
      favoriteCountries,
    };
  },
});
