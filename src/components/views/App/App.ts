import { defineComponent, provide } from 'vue';
import { DefaultApolloClient } from '@vue/apollo-composable';

import apolloClient from '../../../graphql/apollo/client';

import Countries from '../Countries/Countries.vue';

export default defineComponent({
  name: 'App',
  components: {
    Countries,
  },
  setup() {
    provide(DefaultApolloClient, apolloClient);
  },
});
