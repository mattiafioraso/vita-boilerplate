const path = require('path');

require('dotenv').config({ path: path.join(__dirname, '.env.local') });

module.exports = {
  client: {
    service: {
      url: process.env.VITE_GRAPHQL_API,
    },
    includes: [path.join(__dirname, 'src/**/*.graphql')],
  },
};
